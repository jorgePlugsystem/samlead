<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Locatario $locatario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Locatario'), ['action' => 'edit', $locatario->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Locatario'), ['action' => 'delete', $locatario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locatario->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Locatarios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Locatario'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="locatarios view large-9 medium-8 columns content">
    <h3><?= h($locatario->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($locatario->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($locatario->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cpf') ?></th>
            <td><?= h($locatario->cpf) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($locatario->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($locatario->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($locatario->modified) ?></td>
        </tr>
    </table>
</div>
