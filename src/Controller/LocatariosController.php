<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Locatarios Controller
 *
 * @property \App\Model\Table\LocatariosTable $Locatarios
 *
 * @method \App\Model\Entity\Locatario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocatariosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $locatarios = $this->Locatarios->find()->order(["nome" => "ASC"]);

        $this->set(compact('locatarios'));
    }

    /**
     * View method
     *
     * @param string|null $id Locatario id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $locatario = $this->Locatarios->get($id, [
            'contain' => [],
        ]);

        $this->set('locatario', $locatario);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $locatario = $this->Locatarios->newEntity();
        if ($this->request->is('post')) {
            $locatario = $this->Locatarios->patchEntity($locatario, $this->request->getData());
            if ($this->Locatarios->save($locatario)) {
                $this->Flash->success(__('The locatario has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The locatario could not be saved. Please, try again.'));
        }
        $this->set(compact('locatario'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Locatario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $locatario = $this->Locatarios->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $locatario = $this->Locatarios->patchEntity($locatario, $this->request->getData());
            if ($this->Locatarios->save($locatario)) {
                $this->Flash->success(__('The locatario has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The locatario could not be saved. Please, try again.'));
        }
        $this->set(compact('locatario'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Locatario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $locatario = $this->Locatarios->get($id);
        if ($this->Locatarios->delete($locatario)) {
            $this->Flash->success(__('The locatario has been deleted.'));
        } else {
            $this->Flash->error(__('The locatario could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
