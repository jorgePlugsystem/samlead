<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="edit-3"></i></div>
                    <span>Editar Lead</span>
                </h1>
                <div class="page-header-subtitle">ShowAutoMall</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-lg-12">
                <div id="default">
                    <div class="card mb-4">
                        <div class="card-header">Editar Lead</div>
                        <div class="card-body">
                            <?php echo $this->Flash->render(); ?>
                            <div class="sbp-preview">
                                <div class="sbp-preview-content">
                                    <?= $this->Form->create($lead, ['novalidate' => true]) ?>

                                    <div class="form-row">

                                        <div class="col-md-12 mb-12">
                                            <label for="exampleFormControlInput1">Necessidade *</label>
                                            <?= $this->Form->control(
                                                'titulo',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Necessidade',
                                                ]
                                            ); ?>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-row">

                                        <div class="col-md-6 mb-12">
                                            <label for="exampleFormControlInput1">Nome *</label>
                                            <?= $this->Form->control(
                                                'nome_contato',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Nome',
                                                ]
                                            ); ?>

                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Fone *</label>
                                            <?= $this->Form->control(
                                                'fone_contato',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Fone',
                                                ]
                                            ); ?>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Origem *</label>
                                            <?= $this->Form->control(
                                                'origem_id',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => '- Selecione -',
                                                    'options' => $origems,
                                                ]
                                            ); ?>

                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Email </label>
                                            <?= $this->Form->control(
                                                'email_contato',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Email',
                                                ]
                                            ); ?>

                                        </div>

                                    </div>


                                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>