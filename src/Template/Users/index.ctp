<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="book"></i></div>
                    <span>Usuários do Sistema</span>
                </h1>
                <div class="page-header-subtitle">LeadCenter</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card mb-4">
            <div class="card-header">Listagem</div>
            <div class="card-body">
                <?php echo $this->Flash->render(); ?>
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th>Ações</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($users as $user) : ?>
                                <tr>
                                    <td><?= h($user->nome) ?></td>
                                    <td><?= h($user->email) ?></td>
                                    <td><?= h($user->tipo) ?></td>
                                    <td>

                                        <?= $this->Html->link(
                                            '<i data-feather="edit"></i>',
                                            ['action' => 'edit', $user->id],
                                            [
                                                'escape' => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                        <?= $this->Form->postLink(
                                            '<i data-feather="trash-2"></i>',
                                            ['action' => 'delete', $user->id],
                                            [
                                                'confirm' => __('Deseja realmente remover este registro?'),
                                                'escape'   => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</main>