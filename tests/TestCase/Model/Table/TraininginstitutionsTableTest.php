<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TraininginstitutionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TraininginstitutionsTable Test Case
 */
class TraininginstitutionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TraininginstitutionsTable
     */
    public $Traininginstitutions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Traininginstitutions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Traininginstitutions') ? [] : ['className' => TraininginstitutionsTable::class];
        $this->Traininginstitutions = TableRegistry::getTableLocator()->get('Traininginstitutions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Traininginstitutions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
