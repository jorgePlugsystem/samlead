CREATE TABLE `leadcenter`.`empresas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `fone_contato` VARCHAR(45) NOT NULL,
  `nome_contato` VARCHAR(45) NOT NULL,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NULL,
  PRIMARY KEY (`id`));


  CREATE TABLE `leadcenter`.`vendedores` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `fone` VARCHAR(45) NOT NULL,
  `empresa_id` INT NOT NULL,
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `leadcenter`.`leads` 
ADD COLUMN `vendedore_id` INT NULL AFTER `data_destinado`,
ADD COLUMN `empresa_id` INT NOT NULL AFTER `vendedore_id`;

ALTER TABLE `leadcenter`.`users` 
ADD COLUMN `vendedore_id` INT NULL AFTER `empresa_id`;

ALTER TABLE `leadcenter`.`leads` 
ADD COLUMN `is_visualizado` INT NULL DEFAULT 0 AFTER `empresa_id`;
