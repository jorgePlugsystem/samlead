<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Vendedores Controller
 *
 * @property \App\Model\Table\VendedoresTable $Vendedores
 *
 * @method \App\Model\Entity\Vendedore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VendedoresController extends AppController
{

    public function isAuthorized($user)
    {

        // Apenas o proprietário do artigo pode editar e excluí
        // if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
        //     $articleId = (int)$this->request->getParam('pass.0');
        //     if ($this->Articles->isOwnedBy($articleId, $user['id'])) {
        //         return true;
        //     }
        // }

        $acesso = true;
        return $acesso;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $empresa_id = $this->request->getSession()->read('Auth.User.empresa_id');
        $vendedores = $this->Vendedores->find()->where(['empresa_id' => $empresa_id])->order(['nome' => 'ASC']);
        $this->set(compact('vendedores'));
    }

    /**
     * View method
     *
     * @param string|null $id Vendedore id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vendedore = $this->Vendedores->get($id, [
            'contain' => ['Empresas'],
        ]);

        $this->set('vendedore', $vendedore);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vendedore = $this->Vendedores->newEntity();
        if ($this->request->is('post')) {
            $vendedore = $this->Vendedores->patchEntity($vendedore, $this->request->getData());
            if ($this->Vendedores->save($vendedore)) {

                // Cadastro do usuario da empresa

                $this->loadModel("Users");
                $user = $this->Users->newEntity();

                $user->nome = $vendedore->nome;
                $user->email = $vendedore->email;
                $user->password = $vendedore->password;
                $user->tipo = "vendedor";
                $user->empresa_id = $vendedore->empresa_id;
                $user->vendedore_id = $vendedore->id;

                $this->Users->save($user);

                // Cadastro do usuario da empresa

                $this->Flash->success(__('The vendedore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vendedore could not be saved. Please, try again.'));
        }
        $empresas = $this->Vendedores->Empresas->find('list', ['limit' => 200]);
        $this->set(compact('vendedore', 'empresas'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Vendedore id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vendedore = $this->Vendedores->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vendedore = $this->Vendedores->patchEntity($vendedore, $this->request->getData());
            if ($this->Vendedores->save($vendedore)) {
                $this->Flash->success(__('The vendedore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vendedore could not be saved. Please, try again.'));
        }
        $empresas = $this->Vendedores->Empresas->find('list', ['limit' => 200]);
        $this->set(compact('vendedore', 'empresas'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vendedore id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vendedore = $this->Vendedores->get($id);
        if ($this->Vendedores->delete($vendedore)) {

            $this->loadModel("Users");
            $users = $this->Users->find()->where(['empresa_id' => $id]);
            foreach ($users as $user) :
                $this->Users->get($user->id);
                $this->Users->delete($user);
            endforeach;

            $this->Flash->success(__('The vendedore has been deleted.'));
        } else {
            $this->Flash->error(__('The vendedore could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
