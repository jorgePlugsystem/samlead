<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LocatariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LocatariosTable Test Case
 */
class LocatariosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LocatariosTable
     */
    public $Locatarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Locatarios',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Locatarios') ? [] : ['className' => LocatariosTable::class];
        $this->Locatarios = TableRegistry::getTableLocator()->get('Locatarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Locatarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
