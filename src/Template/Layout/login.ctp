<!DOCTYPE html>
<html lang="pt_br">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content />
    <meta name="author" content />
    <title>LeadCenter</title>
    <link href="<?= $this->request->webroot ?>template/css/styles.css" rel="stylesheet" />
    <link rel="icon" type="image/x-icon" href="<?= $this->request->getAttribute("webroot") ?>template/assets/img/favicon.png" />

</head>

<body class="bg-primary">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <?= $this->fetch('content') ?>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="footer mt-auto footer-dark">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 small">Copyright &#xA9; LeadCenter. 2021</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

</body>

</html>