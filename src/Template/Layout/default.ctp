<!DOCTYPE html>
<html lang="pt_br">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content />
    <meta name="author" content />
    <title>LeadCenter</title>
    <link href="<?= $this->request->getAttribute("webroot")  ?>template/css/styles.css" rel="stylesheet" />
    <link href="<?= $this->request->getAttribute("webroot")  ?>template/cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <link rel="icon" type="image/x-icon" href="<?= $this->request->getAttribute("webroot")  ?>template/assets/img/favicon.png" />
    <script data-search-pseudo-elements defer src="<?= $this->request->getAttribute("webroot")  ?>template/cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="<?= $this->request->getAttribute("webroot")  ?>template/cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="nav-fixed">

    <?= $this->element('layout/topnav'); ?>

    <div id="layoutSidenav">
        <?= $this->element('layout/sidenav'); ?>
        <div id="layoutSidenav_content">
            <main>
                <?= $this->fetch('content') ?>
            </main>
            <footer class="footer mt-auto footer-light">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 small">Copyright &#xA9; PlugSystem <?= date('Y') ?></div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/js/scripts.js"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/assets/demo/chart-area-demo.js"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/assets/demo/chart-bar-demo.js"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="<?= $this->request->getAttribute("webroot") ?>template/assets/demo/datatables-demo.js"></script>

    <script src=""></script>
    <?= $this->Html->script('custom.js') ?>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <?= $this->Html->script('masked.js') ?>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
</body>


</html>