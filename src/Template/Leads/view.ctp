<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="edit-3"></i></div>
                    <span>Lead</span>
                </h1>
                <div class="page-header-subtitle">LeadCenter</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-lg-12">
                <div id="default">
                    <div class="card mb-4">
                        <div class="card-header">Lead</div>
                        <div class="card-body">
                            <?php echo $this->Flash->render(); ?>
                            <div class="sbp-preview">
                                <div class="sbp-preview-content">
                                    <table class="table table-bordered table-hover" id="" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Necessidade</th>
                                                <th>Nome</th>
                                                <th>Origem</th>
                                                <th>Fone</th>
                                                <th>Data Cadastro</th>
                                                <th>Status</th>
                                                <th>Locatário</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Necessidade</th>
                                                <th>Nome</th>
                                                <th>Origem</th>
                                                <th>Fone</th>
                                                <th>Data Cadastro</th>
                                                <th>Status</th>
                                                <th>Locatário</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                            <tr>
                                                <td><?= h($lead->titulo) ?></td>
                                                <td><?= h($lead->nome_contato) ?></td>
                                                <td>
                                                    <?= $lead->has('origem') ?
                                                        $lead->origem->origem : 'Sem Origem' ?>

                                                </td>
                                                <td><?= h($lead->fone_cliente) ?></td>
                                                <td><?= h($lead->created) ?></td>
                                                <td>
                                                    <?= $this->element('lead/status', ['status' => $lead->status]); ?>
                                                </td>
                                                <td>
                                                    <?= $lead->has('vendedore') ?
                                                        $lead->vendedore->nome : 'Não Atribuido' ?>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>