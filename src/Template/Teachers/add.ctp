<script src="<?= $this->request->webroot ?>template/code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>

<script type="text/template" id="template-institution">
    <?= $this->element('Teachers/institution'); ?>
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var botaoAdd = $('#AddInstituicao');
        var template = $.trim($('#template-institution').html());
        var nextItem = 1;
        var inputHtml = "";

        var wrapper = $("#institutions");

        botaoAdd.on('click', function(e) {
            e.preventDefault();
            addNewInputToTheForm();
        });

        function addNewInputToTheForm() {

            var id_atual = nextItem;

            var botoesWrap = $('#AddInstituicao').find('#ops');
            inputHtml = template.replace(/::num/g, nextItem++);
            $('#institutions').append(inputHtml);


            function remover(e) {
                e.preventDefault();
                alert("Remover" + nextItem);
                //$("#linha"+nextItem).remove();
            }


            $(this).on("click", ".remove_field", function(e) { //user click on remove text

                e.preventDefault();
                linha = $(this).attr('id');
                $("#" + linha).remove();

            });

            $("#estado").on('change', function(e) {

                var id = $(this).val();

                $.ajax({
                    type: "GET",
                    data: {
                        id: id
                    },
                    url: "<?php echo \Cake\Routing\Router::url(array('controller' => 'Teachers', 'action' => 'listarCidadesPorEstado')); ?>",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    },
                    success: function(html) {
                        console.log(html);
                        //$("#caixa_preloader").hide();
                        //$("#caixa_subcategoria_id").show();
                        $.each(html, function(key, value) {
                            //console.log(value.nome + value.nome);
                            $('<option>').val(value.nome).text(value.nome).appendTo($("#cidade"));
                        });
                    },
                    error: function(tab) {
                        alert('error: ' + tab.statusText);
                    },
                });


            });


        }


    });
</script>

<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="edit-3"></i></div>
                    <span>Novo Professor</span>
                </h1>
                <div class="page-header-subtitle">Observatório da Formação Docente Continuada</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-lg-12">
                <div id="default">
                    <div class="card mb-4">
                        <div class="card-header">Inserir Novo Professor</div>
                        <div class="card-body">
                            <?php echo $this->Flash->render(); ?>
                            <div class="sbp-preview">
                                <div class="sbp-preview-content">
                                    <?= $this->Form->create($teacher, ['novalidate' => true]) ?>

                                    <div class="form-row">

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Nome *</label>
                                            <?= $this->Form->control(
                                                'nome',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Nome Completo',
                                                ]
                                            ); ?>

                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Email *</label>
                                            <?= $this->Form->control(
                                                'email',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Email',
                                                ]
                                            ); ?>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">CPF *</label>
                                            <?= $this->Form->control(
                                                'cpf',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control cpf',
                                                    'placeholder' => 'Inserir CPF Válido',
                                                ]
                                            ); ?>

                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Data Nascimento *</label>
                                            <?= $this->Form->control(
                                                'data_nascimento',
                                                [
                                                    'type' => 'text',
                                                    'id' => '',
                                                    'label' => false,
                                                    'class' => 'form-control calendario',
                                                    'placeholder' => 'Inserir Data de Nascimento',
                                                ]
                                            ); ?>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="col-md-4 mb-3">
                                            <label for="exampleFormControlInput1">Formação Academica *</label>
                                            <?= $this->Form->control(
                                                'formacao_academica',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => '- Selecione -',
                                                    'options' => [
                                                        'Graduação' => 'Graduação',
                                                        'Especialização' => 'Especialização',
                                                        'Mestrado' => 'Mestrado',
                                                        'Doutorado' => 'Doutorado',
                                                    ],
                                                ]
                                            ); ?>

                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-lg-12">
                                            <label for="">Adicionar Instituição de Ensino</label><br>
                                            <input type="button" id="AddInstituicao" value="Adicionar Instituição" class="btn btn-warning">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-lg-12">
                                            <div id="institutions"></div>
                                        </div>
                                    </div>

                                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>