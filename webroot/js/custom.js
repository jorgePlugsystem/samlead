$(function() {
    //teste
    $(".calendario").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        yearRange: "-100:+0",
        dateFormat: "dd-mm-yy",
        dayNames: [
            "Domingo",
            "Segunda",
            "Terça",
            "Quarta",
            "Quinta",
            "Sexta",
            "Sábado"
        ],
        dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S", "D"],
        dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
        monthNames: [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"
        ],
        monthNamesShort: [
            "Jan",
            "Fev",
            "Mar",
            "Abr",
            "Mai",
            "Jun",
            "Jul",
            "Ago",
            "Set",
            "Out",
            "Nov",
            "Dez"
        ],
        nextText: "Próximo",
        prevText: "Anterior",
        closeText: "Fechar",
        currentText: "Hoje"
    });

    $(".cpf").mask("999.999.999-99", { placeholder: "000.000.000-00" });
    $(".cnpj").mask("99.999.999/9999-99", {
        placeholder: "00.000.000/0000-00"
    });
    $(".fone").mask("(99)99999-9999", { placeholder: "(99)99999-9999" });
});
