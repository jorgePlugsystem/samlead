<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="edit-3"></i></div>
                    <span>Novo Contrato</span>
                </h1>
                <div class="page-header-subtitle">ShowAutoMall</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-lg-12">
                <div id="default">
                    <div class="card mb-4">
                        <div class="card-header">Inserir Novo Contrato</div>
                        <div class="card-body">
                            <?php echo $this->Flash->render(); ?>
                            <div class="sbp-preview">
                                <div class="sbp-preview-content">
                                    <?= $this->Form->create($contrato, ['novalidate' => true]) ?>

                                    <?= $this->Form->hidden('locatario_id', ['value' => $locatario_id]); ?>

                                    <div class="form-row">

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Data Inicio *</label>
                                            <?= $this->Form->control(
                                                'data_inicio',
                                                [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control calendario',
                                                    'placeholder' => 'Inserir Data Inicial',
                                                ]
                                            ); ?>

                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Data Fim *</label>
                                            <?= $this->Form->control(
                                                'data_fim',
                                                [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control calendario',
                                                    'placeholder' => 'Inserir Data Final',
                                                ]
                                            ); ?>

                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Valor *</label>
                                            <?= $this->Form->control(
                                                'valor',
                                                [
                                                    'type' => 'text',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Inserir Valor',
                                                ]
                                            ); ?>

                                        </div>

                                    </div>

                                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>