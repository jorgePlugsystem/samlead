<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
/**
 * Created by PhpStorm.
 * User: jorgemedeiros
 * Date: 03/03/16
 * Time: 09:04
 */
class MoneyFormatterBehavior extends Behavior{

    

    public function formatarMoedaBr($model,$num) {
        if(isset($num) && !empty($num)){
            $valorFormatado = 'R$ ' . number_format($num, 2, ',', '.');
            return $valorFormatado;
        }else{
            return "";
        }
    }

    /**
     * @author jorge medeiros
     * [ - Método Para Formatar qualquer R$ Da String  -
     *
     */
    public function moeda($get_valor) {
        $get_valor = substr($get_valor,2);
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        return $valor; //retorna o valor formatado para gravar no banco
    }


}