<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Leads Controller
 *
 * @property \App\Model\Table\LeadsTable $Leads
 *
 * @method \App\Model\Entity\Lead[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LeadsController extends AppController
{

    public function isAuthorized($user)
    {
        $acesso = true;
        return $acesso;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $this->loadModel("Origems");
        $origems = $this->Origems->find('list');

        $tipo_user = $this->request->getSession()->read('Auth.User.tipo');
        $empresa_id = $this->request->getSession()->read('Auth.User.empresa_id');
        $vendedore_id = $this->request->getSession()->read('Auth.User.vendedore_id');
        $user_id = $this->request->getSession()->read('Auth.User.id');

        $data_inicial = (!empty($this->request->getQuery('data_inicio'))) ? $this->request->getQuery('data_inicio') : '';
        $data_final = (!empty($this->request->getQuery('data_fim'))) ? $this->request->getQuery('data_fim') : '';
        $titulo = (!empty($this->request->getQuery('titulo'))) ? $this->request->getQuery('titulo') : '';
        $nome_cliente = (!empty($this->request->getQuery('nome_cliente'))) ? $this->request->getQuery('nome_cliente') : '';
        $fone_cliente = (!empty($this->request->getQuery('fone_cliente'))) ? $this->request->getQuery('fone_cliente') : '';
        $origem_id = (!empty($this->request->getQuery('origem_id'))) ? $this->request->getQuery('origem_id') : '';
        $email_cliente = (!empty($this->request->getQuery('email_cliente'))) ? $this->request->getQuery('email_cliente') : '';
        $status = (!empty($this->request->getQuery('status'))) ? $this->request->getQuery('status') : '';

        if ($tipo_user == "empresa") {
            $leads = $this->Leads->find()->contain(['Vendedores'])->where(['leads.empresa_id' => $empresa_id]);
        } elseif ($tipo_user == "vendedor") {
            $leads = $this->Leads->find()->contain(['Vendedores'])
                ->where([
                    'leads.empresa_id' => $empresa_id,
                    'leads.vendedore_id' => $vendedore_id
                ]);
        }



        if (!empty($data_inicial) && empty($data_final)) {
            $data_inicial = date('Y-m-d',  strtotime($data_inicial));
            $leads->where([
                'Leads.cadastrado' => $data_inicial,
            ]);
        }

        if (!empty($data_inicial) && !empty($data_final)) {
            $data_inicio = date('Y-m-d',  strtotime($data_inicial));
            $data_fim = date('Y-m-d',  strtotime($data_final));
            $leads->where([
                'Leads.cadastrado BETWEEN :start AND :end',
            ])
                ->bind(':start', new \DateTime($data_inicio), 'date')
                ->bind(':end',   new \DateTime($data_fim), 'date');
        }

        if (isset($titulo) && !empty($titulo)) {
            $leads->where(['Leads.titulo' => $titulo]);
        }

        if (isset($nome_cliente) && !empty($nome_cliente)) {
            $leads->where(['Leads.nome_cliente LIKE' => "%$nome_cliente%"]);
        }

        if (isset($fone_cliente) && !empty($fone_cliente)) {
            $leads->where(['Leads.fone_cliente' => $fone_cliente]);
        }

        if (isset($origem_id) && !empty($origem_id)) {
            $leads->where(['Leads.origem_id' => $origem_id]);
        }

        if (isset($email_cliente) && !empty($email_cliente)) {
            $leads->where(['Leads.email_cliente' => $email_cliente]);
        }

        if (isset($status) && !empty($status)) {
            $leads->where(['Leads.status' => $status]);
        }

        $leads->contain(['Origems']);
        $leads->order(['Leads.created' => 'desc']);

        //$leads = $this->paginate($leads);

        $this->set(compact('leads', 'origems'));
    }



    public function encaminhar($id)
    {

        $this->loadModel("Vendedores");

        $lead = $this->Leads->get($id, [
            'contain' => ['Origems'],
        ]);

        $empresa_id = $this->request->getSession()->read('Auth.User.empresa_id');
        //$id = (!empty($this->request->query('lead_id'))) ? $this->request->query('lead_id') : '';

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->autoRender = false;
            $lead = $this->Leads->patchEntity($lead, $this->request->getData());
            $lead->status = 1;
            if ($this->Leads->save($lead)) {
                $this->Flash->success(__('Lead encaminhado com sucesso!'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O Lead não foi encaminhado.'));
        }

        $vendedores = $this->Vendedores->find('list')->where(['empresa_id' => $empresa_id]);
        $this->set(compact('vendedores', 'lead'));
    }

    /**
     * View method
     *
     * @param string|null $id Lead id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lead = $this->Leads->get($id, [
            'contain' => ["Origems", "Vendedores"],
        ]);

        $lead->is_visualizado = true;

        $this->Leads->save($lead);

        $this->set('lead', $lead);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lead = $this->Leads->newEntity();
        if ($this->request->is('post')) {
            $lead = $this->Leads->patchEntity($lead, $this->request->getData());
            $lead->status = false;
            $lead->cadastrado = date("Y-m-d");
            if ($this->Leads->save($lead)) {
                $lead = $this->Leads->patchEntity($lead, $this->request->getData());
                if ($this->Leads->save($lead)) {
                    $this->Flash->success(__('Lead adicionado com sucesso!'));

                    return $this->redirect(['action' => 'add']);
                }
                $this->Flash->error(__('O Lead não foi alterado.'));
            }
            $this->Flash->error(__('O Lead não foi salvo.'));
        }

        $this->loadModel("Origems");
        $origems = $this->Origems->find('list');

        $this->set(compact('lead', 'origems'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Lead id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lead = $this->Leads->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lead = $this->Leads->patchEntity($lead, $this->request->getData());
            if ($this->Leads->save($lead)) {
                $this->Flash->success(__('Lead alterado com sucesso!'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O Lead não foi alterado.'));
        }
        $this->loadModel("Origems");
        $origems = $this->Origems->find('list');

        $this->set(compact('lead', 'origems'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Lead id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lead = $this->Leads->get($id);
        if ($this->Leads->delete($lead)) {
            $this->Flash->success(__('Lead removido com sucesso!'));
        } else {
            $this->Flash->error(__('O Lead não foi removido.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
