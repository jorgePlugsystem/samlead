<main>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header justify-content-center">
                        <h3 class="font-weight-light my-4">Login</h3>
                    </div>
                    <div class="card-body">
                        <?php echo $this->Flash->render(); ?>
                        <?= $this->Form->create() ?>
                        <div class="form-group">
                            <label class="small mb-1" for="inputEmailAddress">Email</label>
                            <?= $this->Form->control(
                                'email',
                                [
                                    'label' => false,
                                    'class' => 'form-control py-4',
                                    'placeholder' => 'Inserir Email',
                                ]
                            ); ?>

                        </div>
                        <div class="form-group">
                            <label class="small mb-1" for="inputPassword">Senha</label>
                            <?= $this->Form->control(
                                'password',
                                [
                                    'label' => false,
                                    'class' => 'form-control py-4',
                                    'placeholder' => 'Inserir senha',
                                ]
                            ); ?>

                        </div>
                        <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                            <a class="small" href="<?= $this->request->getAttribute("webroot")  ?>recuperarsenha">Perdeu sua Senha?
                            </a>
                            <?= $this->Form->button(__('Acessar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                    <div class="card-footer text-center">
                        <div class="small">
                            <a href="<?= $this->request->getAttribute("webroot")  ?>quemsoueu">Cadastre-se!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>