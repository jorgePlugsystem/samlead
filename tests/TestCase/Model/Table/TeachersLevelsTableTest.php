<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TeachersLevelsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TeachersLevelsTable Test Case
 */
class TeachersLevelsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TeachersLevelsTable
     */
    public $TeachersLevels;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TeachersLevels',
        'app.Teachers',
        'app.Levels',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TeachersLevels') ? [] : ['className' => TeachersLevelsTable::class];
        $this->TeachersLevels = TableRegistry::getTableLocator()->get('TeachersLevels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TeachersLevels);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
