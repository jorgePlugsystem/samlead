<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Origems Controller
 *
 * @property \App\Model\Table\OrigemsTable $Origems
 *
 * @method \App\Model\Entity\Origem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrigemsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $origems = $this->paginate($this->Origems);

        $this->set(compact('origems'));
    }

    /**
     * View method
     *
     * @param string|null $id Origem id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $origem = $this->Origems->get($id, [
            'contain' => [],
        ]);

        $this->set('origem', $origem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $origem = $this->Origems->newEntity();
        if ($this->request->is('post')) {
            $origem = $this->Origems->patchEntity($origem, $this->request->getData());
            if ($this->Origems->save($origem)) {
                $this->Flash->success(__('The origem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The origem could not be saved. Please, try again.'));
        }
        $this->set(compact('origem'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Origem id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $origem = $this->Origems->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $origem = $this->Origems->patchEntity($origem, $this->request->getData());
            if ($this->Origems->save($origem)) {
                $this->Flash->success(__('The origem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The origem could not be saved. Please, try again.'));
        }
        $this->set(compact('origem'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Origem id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $origem = $this->Origems->get($id);
        if ($this->Origems->delete($origem)) {
            $this->Flash->success(__('The origem has been deleted.'));
        } else {
            $this->Flash->error(__('The origem could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
