<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="book"></i></div>
                    <span>Professores</span>
                </h1>
                <div class="page-header-subtitle">Observatório da Formação Docente Continuada</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card mb-4">
            <div class="card-header">Listagem</div>
            <div class="card-body">
                <?php echo $this->Flash->render(); ?>
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>CPF</th>
                                <th>Data Nascimento</th>
                                <th style="text-align: center;">Formação Acadêmica</th>
                                <th style="text-align: center;">Instituições</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>CPF</th>
                                <th>Data Nascimento</th>
                                <th style="text-align: center;">Formação Academica</th>
                                <th style="text-align: center;">Instituições</th>
                                <th>Ações</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($teachers as $teacher) : ?>
                                <tr>
                                    <td><?= h($teacher->nome) ?></td>
                                    <td><?= h($teacher->email) ?></td>
                                    <td><?= h($teacher->cpf) ?></td>
                                    <td><?= $teacher->data_nascimento ?></td>
                                    <td style="text-align: center;"><?= $this->cell('Teacher::formacao', [$teacher->formacao_academica]); ?></td>
                                    <td style="text-align: center;">
                                        <?php foreach ($teacher->institutions as $institution) : ?>
                                            <p><b>Tipo:</b> <?= $institution->tipo ?></p>
                                            <p><b>Cidade:</b> <?= $institution->cidade ?></p>
                                            <p><b>Cidade:</b> <?= $institution->estado ?></p>
                                            <p><b>Bairro / Zona:</b> <?= $institution->bairro ?></p>
                                            <hr>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>

                                        <?= $this->Html->link(
                                            '<i data-feather="edit"></i>',
                                            ['action' => 'edit', $teacher->id],
                                            [
                                                'escape' => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                        <?= $this->Form->postLink(
                                            '<i data-feather="trash-2"></i>',
                                            ['action' => 'delete', $teacher->id],
                                            [
                                                'confirm' => __('Deseja realmente remover este registro?'),
                                                'escape'   => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</main>