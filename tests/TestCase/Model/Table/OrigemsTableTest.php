<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrigemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrigemsTable Test Case
 */
class OrigemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrigemsTable
     */
    public $Origems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Origems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Origems') ? [] : ['className' => OrigemsTable::class];
        $this->Origems = TableRegistry::getTableLocator()->get('Origems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Origems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
