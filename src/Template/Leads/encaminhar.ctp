<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="edit-3"></i></div>
                    <span>Encaminhar Lead</span>
                </h1>
                <div class="page-header-subtitle">LeadCenter</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-lg-12">
                <div id="default">
                    <div class="card mb-4">
                        <div class="card-header">Encaminhar Lead</div>
                        <div class="card-body">
                            <?php echo $this->Flash->render(); ?>
                            <div class="sbp-preview">
                                <div class="sbp-preview-content">
                                    <?= $this->Form->create($lead, ['url' => ['action' => 'encaminhar'], 'type' => 'post', 'role' => 'form', 'novalidate' => true]) ?>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-4">
                                            <p><b>Necessidade:</b> <?= $lead->titulo ?></p>
                                            <p><b>Nome do Contato:</b> <?= $lead->nome_contato ?></p>
                                            <p><b>Origem:</b> <?= $lead->has('origem') ? $lead->origem->origem : 'Sem Origem' ?></p>
                                            <p><b>Fone:</b> <?= $lead->fone_contato ?></p>
                                            <p><b>Data de Cadastro:</b> <?= h($lead->cadastrado) ?></p>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-4">
                                            <label for="exampleFormControlInput1">Vendedor *</label>
                                            <?= $this->Form->control(
                                                'vendedore_id',
                                                [
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'empty' => '- Selecione -',
                                                    'options' => $vendedores,
                                                ]
                                            ); ?>
                                        </div>
                                    </div>
                                    <?= $this->Form->button(__('Encaminhar'), ['class' => 'btn btn-primary']) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>