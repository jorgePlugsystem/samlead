<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;


/**
 * Created by PhpStorm.
 * User: jorgemedeiros
 * Date: 26/02/16
 * Time: 12:48
 */
class DateFormatterBehavior extends Behavior{


    public function setup(Model $Model, $settings = array()) {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array(
                'option1_key' => 'option1_default_value',
                'option2_key' => 'option2_default_value',
                'option3_key' => 'option3_default_value',
            );
        }
        $this->settings[$Model->alias] = array_merge(
            $this->settings[$Model->alias], (array)$settings);
    }

    public function formatar($data) {

        $data_formatada = "";

        if(!empty($data)){
            $data_formatada = date("Y-m-d", strtotime($data));
        }      

        return $data_formatada;

    }


}