<?php
use Migrations\AbstractMigration;

class CreateEmpresas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empresas');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('cpf', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('cnpj', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('locatario_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addIndex([
            'nome',
        ], [
            'name' => 'UNIQUE_NOME',
            'unique' => true,
        ]);
        $table->addIndex([
            'cpf',
        ], [
            'name' => 'UNIQUE_CPF',
            'unique' => true,
        ]);
        $table->addIndex([
            'cnpj',
        ], [
            'name' => 'UNIQUE_CNPJ',
            'unique' => true,
        ]);
        $table->create();
    }
}
