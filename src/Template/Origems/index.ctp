<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="book"></i></div>
                    <span>Origens</span>
                </h1>
                <div class="page-header-subtitle">ShowAutoMall</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card mb-4">
            <div class="card-header">Listagem</div>
            <div class="card-body">
                <?php echo $this->Flash->render(); ?>
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Origem</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Origem</th>
                                <th>Ações</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($origems as $origem) : ?>
                                <tr>
                                    <td><?= h($origem->origem) ?></td>
                                    <td>

                                        <?= $this->Html->link(
                                            '<i data-feather="edit"></i>',
                                            ['action' => 'edit', $origem->id],
                                            [
                                                'escape' => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                        <?= $this->Form->postLink(
                                            '<i data-feather="trash-2"></i>',
                                            ['action' => 'delete', $origem->id],
                                            [
                                                'confirm' => __('Deseja realmente remover este registro?'),
                                                'escape'   => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</main>