<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Origem $origem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Origem'), ['action' => 'edit', $origem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Origem'), ['action' => 'delete', $origem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $origem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Origems'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Origem'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="origems view large-9 medium-8 columns content">
    <h3><?= h($origem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Origem') ?></th>
            <td><?= h($origem->origem) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($origem->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($origem->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($origem->modified) ?></td>
        </tr>
    </table>
</div>
