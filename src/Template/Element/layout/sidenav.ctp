<div id="layoutSidenav_nav">
    <nav class="sidenav shadow-right sidenav-light">
        <div class="sidenav-menu">
            <div class="nav accordion" id="accordionSidenav">
                <div class="sidenav-menu-heading">Gerenciamento</div>
                <?php if ($this->request->getSession()->read('Auth.User.tipo') == "root") : ?>
                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapseempresa" aria-expanded="false" aria-controls="collapseempresa">
                        <div class="nav-link-icon"><i data-feather="activity"></i></div>
                        Empresas
                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseempresa" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>empresas/add">Nova Empresa</a>
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>empresas">Listar Empresas</a>
                        </nav>
                    </div>
                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapseOrigem" aria-expanded="false" aria-controls="collapseOrigem">
                        <div class="nav-link-icon"><i data-feather="activity"></i></div>
                        Origens
                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseOrigem" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>origems/add">Nova Origem</a>
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>origems">Listar Origens</a>
                        </nav>
                    </div>
                <?php endif; ?>
                <?php if ($this->request->getSession()->read('Auth.User.tipo') == "empresa" || $this->request->getSession()->read('Auth.User.tipo') == "vendedor") : ?>
                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapseLeads" aria-expanded="false" aria-controls="collapseLeads">
                        <div class="nav-link-icon"><i data-feather="activity"></i></div>
                        Leads
                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseLeads" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <?php if ($this->request->getSession()->read('Auth.User.tipo') == "empresa") : ?>
                                <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>leads/add">Novo Lead</a>
                            <?php endif; ?>
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>leads">Listar Leads</a>
                        </nav>
                    </div>
                <?php endif; ?>
                <?php if ($this->request->getSession()->read('Auth.User.tipo') == "empresa") : ?>
                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapsevendedor" aria-expanded="false" aria-controls="collapsevendedor">
                        <div class="nav-link-icon"><i data-feather="activity"></i></div>
                        Vendedores
                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapsevendedor" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>vendedores/add">Novo Vendedor</a>
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>vendedores">Listar Vendedores</a>
                        </nav>
                    </div>
                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapsecliente" aria-expanded="false" aria-controls="collapsecliente">
                        <div class="nav-link-icon"><i data-feather="activity"></i></div>
                        Clientes
                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapsecliente" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>clientes/add">Novo Cliente</a>
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>clientes">Listar Clientes</a>
                        </nav>
                    </div>
                    <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapseadm" aria-expanded="false" aria-controls="collapseadm">
                        <div class="nav-link-icon"><i data-feather="activity"></i></div>
                        Usuários
                        <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseadm" data-parent="#accordionSidenav">
                        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>users/add">Novo Usuário</a>
                            <a class="nav-link" href="<?= $this->request->getAttribute("webroot") ?>users">Listar Usuários</a>
                        </nav>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="sidenav-footer">
            <div class="sidenav-footer-content">

            </div>
        </div>
    </nav>
</div>