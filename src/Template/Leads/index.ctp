<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="book"></i></div>
                    <span>Leads</span>
                </h1>
                <div class="page-header-subtitle">LeadCenter</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">

        <div class="card mb-4">
            <div class="card-header">Filtro</div>
            <div class="card-body">
                <div class="sbp-preview">
                    <div class="sbp-preview-content">
                        <?= $this->Form->create(null, ['url' => ['action' => 'index'], 'type' => 'get', 'role' => 'form', 'novalidate' => true]) ?>
                        <div class="form-row">

                            <div class="col-md-6 mb-12">
                                <label for="exampleFormControlInput1">Data Inicial *</label>
                                <?= $this->Form->control(
                                    'data_inicio',
                                    [
                                        'label' => false,
                                        'class' => 'form-control calendario',
                                        'placeholder' => 'Data Inicial da Pesquisa',
                                    ]
                                ); ?>

                            </div>

                            <div class="col-md-6 mb-4">
                                <label for="exampleFormControlInput1">Data Final *</label>
                                <?= $this->Form->control(
                                    'data_fim',
                                    [
                                        'label' => false,
                                        'class' => 'form-control calendario',
                                        'placeholder' => 'Data Final da Pesquisa',
                                    ]
                                ); ?>

                            </div>
                        </div>
                        <br>

                        <div class="form-row">

                            <div class="col-md-12 mb-12">
                                <label for="exampleFormControlInput1">Necessidade *</label>
                                <?= $this->Form->control(
                                    'titulo',
                                    [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'Inserir Necessidade',
                                    ]
                                ); ?>

                            </div>
                        </div>
                        <br>
                        <div class="form-row">

                            <div class="col-md-6 mb-12">
                                <label for="exampleFormControlInput1">Nome *</label>
                                <?= $this->Form->control(
                                    'nome_cliente',
                                    [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'Inserir Nome',
                                    ]
                                ); ?>

                            </div>

                            <div class="col-md-6 mb-4">
                                <label for="exampleFormControlInput1">Fone *</label>
                                <?= $this->Form->control(
                                    'fone_cliente',
                                    [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'Inserir Fone',
                                    ]
                                ); ?>

                            </div>
                        </div>
                        <br>
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <label for="exampleFormControlInput1">Origem *</label>
                                <?= $this->Form->control(
                                    'origem_id',
                                    [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'empty' => '- Selecione -',
                                        'options' => $origems,
                                    ]
                                ); ?>

                            </div>

                            <div class="col-md-6 mb-4">
                                <label for="exampleFormControlInput1">Email </label>
                                <?= $this->Form->control(
                                    'email_cliente',
                                    [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'placeholder' => 'Inserir Email',
                                    ]
                                ); ?>

                            </div>

                            <div class="col-md-6 mb-4">
                                <label for="exampleFormControlInput1">Status </label>
                                <?= $this->Form->control(
                                    'status',
                                    [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'empty' => '- Selecione -',
                                        'options' => [0 => "Inativo", 1 => "Encaminhado"],
                                    ]
                                ); ?>

                            </div>

                        </div>


                        <?= $this->Form->button(__('Pesquisar'), ['class' => 'btn btn-warning']) ?>
                        <?= $this->Html->link(__('Limpar Busca'), ['action' => 'index'], ['class' => 'btn btn-info']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-header">Listagem</div>
            <div class="card-body">
                <?php echo $this->Flash->render(); ?>
                <div class="datatable table-responsive">
                    <table class="table table-bordered table-hover" id="" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Necessidade</th>
                                <th>Nome</th>
                                <th>Origem</th>
                                <th>Fone</th>
                                <th>Data Cadastro</th>
                                <th>Status</th>
                                <th>Visto</th>
                                <th>Vendedor</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Necessidade</th>
                                <th>Nome</th>
                                <th>Origem</th>
                                <th>Fone</th>
                                <th>Data Cadastro</th>
                                <th>Status</th>
                                <th>Visto</th>
                                <th>Vendedor</th>
                                <th>Ações</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($leads as $lead) : ?>
                                <tr>
                                    <td><?= h($lead->titulo) ?></td>
                                    <td>
                                        <?= $lead->is_visualizado ?
                                            h($lead->nome_contato) : '<span class="badge badge-danger">Bloqueado</span>' ?>
                                    </td>
                                    <td>
                                        <?= $lead->has('origem') ?
                                            $lead->origem->origem : 'Sem Origem' ?>

                                    </td>
                                    <td>
                                        <?= $lead->is_visualizado ?
                                            h($lead->fone_contato) : '<span class="badge badge-danger">Bloqueado</span>' ?>
                                    </td>
                                    <td><?= h($lead->created) ?></td>
                                    <td>
                                        <?= $this->element('lead/status', ['status' => $lead->status]); ?>
                                    </td>
                                    <td>
                                        <?= $lead->is_visualizado ?
                                            '<span class="badge badge-success">Visualizado</span>' : '<span class="badge badge-danger">Não Visualizado</span>' ?>

                                    </td>
                                    <td>
                                        <?= $lead->has('vendedore') ?
                                            $lead->vendedore->nome : 'Não Atribuido' ?>

                                    </td>
                                    <td>

                                        <?= $this->Html->link(
                                            '<i data-feather="search"></i>',
                                            ['action' => 'view', $lead->id],
                                            [
                                                'escape' => false,
                                                'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                            ]
                                        ) ?>

                                        <?php if ($this->request->getSession()->read('Auth.User.tipo') == "empresa") : ?>
                                            <?= $this->Html->link(
                                                '<i data-feather="user-plus"></i>',
                                                ['action' => 'encaminhar', $lead->id],
                                                [
                                                    'escape' => false,
                                                    'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                                ]
                                            ) ?>

                                            <?= $this->Html->link(
                                                '<i data-feather="edit"></i>',
                                                ['action' => 'edit', $lead->id],
                                                [
                                                    'escape' => false,
                                                    'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                                ]
                                            ) ?>

                                            <?= $this->Form->postLink(
                                                '<i data-feather="trash-2"></i>',
                                                ['action' => 'delete', $lead->id],
                                                [
                                                    'confirm' => __('Deseja realmente remover este registro?'),
                                                    'escape'   => false,
                                                    'class' => 'btn btn-datatable btn-icon btn-transparent-dark'
                                                ]
                                            ) ?>
                                        <?php endif; ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>